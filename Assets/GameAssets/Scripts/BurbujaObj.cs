﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BurbujaObj : MonoBehaviour {
	
	public string Resultado;//es la respuesta del problema en el objeto
	private Text RespuestaEscrita;// es la respuesta que va a aparecer cuando den enter

	void Start () {
		GameObject Respuesta = GameObject.Find ("RespuestaEscrita");
		RespuestaEscrita = Respuesta.GetComponent<Text>();
		//rigidbody2D.AddForce(new Vector2 (0, -1) * ControlBurbujas.BurbujaSpeed);
	}

	void Update () {
		if(Resultado == RespuestaEscrita.text){
			DestroyObject(gameObject);
			ControlBurbujas.puntuacion ++;
			SoundManager.instance.AcertarGis();

			if(ControlBurbujas.PowerID == 2) //cuando esta activado el poder double te da un punto mas
				ControlBurbujas.puntuacion ++;
		}

		if(RespuestaEscrita.text == "DELALL" ){
			DestroyObject(gameObject);
			ControlBurbujas.puntuacion ++;
		}

		if (ControlBurbujas.PowerID == 3) {//congelar
			rigidbody2D.velocity = new Vector2(0,0);
		}

		if (ControlBurbujas.PowerID == 1) {//Slow
			rigidbody2D.velocity = new Vector2(0,0);
			rigidbody2D.AddForce(new Vector2 (0, -1) * ControlBurbujas.BurbujaSpeed);
		}

		if (ControlBurbujas.PowerID == 0 || ControlBurbujas.PowerID == 2) {
			rigidbody2D.velocity = new Vector2(0,0);
			rigidbody2D.AddForce(new Vector2 (0, -1) * ControlBurbujas.BurbujaSpeed);
		}

		if (ControlBurbujas.GameOver == 1) {
			DestroyObject (gameObject);
			SoundManager.instance.Wooshhh();
		}
	}

	void OnTriggerEnter2D(Collider2D hitObj){
		if (hitObj.gameObject.tag == "EndLine") {
			DestroyObject(gameObject);
			SoundManager.instance.VidaMenos();
			ControlBurbujas.vida --;
		}
	}
}
