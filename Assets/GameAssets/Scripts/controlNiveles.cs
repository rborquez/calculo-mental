﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class controlNiveles : MonoBehaviour {

	public Button Nivel2B;
	public Button Nivel3B;
	public Text Text1;
	public Text Text2;
	public Text Record1;
	public Text Record2;
	public Text Record3;

	// Use this for initialization
	void Start () {
		if (PlayerPrefs.GetInt ("RECORD1") >= 40) {
			Nivel2B.interactable = true;
			Text1.enabled = false;
		}else 
			Nivel2B.interactable = false;
		

		if (PlayerPrefs.GetInt ("RECORD2") >= 40) {
			Nivel3B.interactable = true;
			Text2.enabled = false;
		}else
			Nivel3B.interactable = false;

		
	}

	void Update () {
		Record1.text = ("" + PlayerPrefs.GetInt ("RECORD1"));
		Record2.text = ("" + PlayerPrefs.GetInt ("RECORD2")); 
		Record3.text = ("" + PlayerPrefs.GetInt ("RECORD3")); 
	}

	public void Nivel1(){
		Application.LoadLevel ("MainGame");
	}

	public void Nivel2(){
		Application.LoadLevel ("Nivel2");
	}
	public void Nivel3(){
		Application.LoadLevel ("Nivel3");
	}

	public void Back(){
		Application.LoadLevel("MainMenu");
	}

}
