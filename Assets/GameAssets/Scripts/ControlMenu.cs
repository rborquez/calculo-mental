﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlMenu : MonoBehaviour {

	public Text Record1;
	public Text Record2;
	public Text Record3;
	public Animator avion1;
	public Animator avion2;
	void Start () {
		avion1.SetBool ("volando", true);
		avion2.SetBool ("volando", true);
		StartCoroutine (avionesVolando());
	}

	void Update () {
		Record1.text = ("" + PlayerPrefs.GetInt ("RECORD1"));
		Record2.text = ("" + PlayerPrefs.GetInt ("RECORD2")); 
		Record3.text = ("" + PlayerPrefs.GetInt ("RECORD3")); 
	}

	public void Jugar(){
		Application.LoadLevel ("SelectLvl");
	}

	public void Amigos(){
		Application.LoadLevel ("Amigos");
	}

	public void Opciones(){
		Application.LoadLevel ("Opciones");
	}

	private IEnumerator avionesVolando(){
		yield return new WaitForSeconds (9);
		avion1.SetBool ("volando", false);
		avion2.SetBool ("volando", false);
		yield return new WaitForSeconds (1);
		avion1.SetBool ("volando", true);
		avion2.SetBool ("volando", true);
		StartCoroutine (avionesVolando());
	}

}
