﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ControlOpciones : MonoBehaviour {

	public Image CredImg;
	public Image CredBot;
	public Text CredTxt;
	public Image AyuImg;
	public Image AyuTxt;
	public Image AyuBot;

	void Start () {
		CredImg.enabled = false;
		CredBot.enabled = false;
		CredTxt.enabled = false;
		AyuImg.enabled = false;
		AyuTxt.enabled = false;
		AyuBot.enabled = false;
	}

	public void creditos(){
		CredImg.enabled = true;
		CredTxt.enabled = true;
		CredBot.enabled = true;
	}

	public void tutorial(){
		Application.LoadLevel ("Instrucciones");
	}

	public void Ayuda(){
		AyuImg.enabled = true;
		AyuTxt.enabled = true;
		AyuBot.enabled = true;
	}

	public void CerrarAyu(){
		AyuImg.enabled = false;
		AyuTxt.enabled = false;
		AyuBot.enabled = false;
	}

	public void CerrarCred(){
		CredImg.enabled = false;
		CredTxt.enabled = false;
		CredBot.enabled = false;
	}

	public void Salir(){
		Application.LoadLevel ("MainMenu");
	}
}
