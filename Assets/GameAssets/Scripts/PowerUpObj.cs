﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PowerUpObj : MonoBehaviour {


	public string PowerName;
	private ControlBurbujas ControlBurb;
	void Start () {
		GameObject Control = GameObject.Find ("Spawner");
		ControlBurb = Control.GetComponent<ControlBurbujas>();
		rigidbody2D.AddForce(new Vector2 (0, -1) * ControlBurbujas.BurbujaSpeed * 1.5f);
	}
	
	void Update () {
		if(ControlBurbujas.GameOver == 1)
			DestroyObject(gameObject);
	}

	void OnMouseDown (){
		switch (PowerName) {
		case "freeze":
			ControlBurbujas.PowerID = 3;
			ControlBurb.PowerUpFreeze();
			DestroyObject(gameObject);
			break;
		case "double":
			ControlBurbujas.PowerID = 2;
			ControlBurb.PowerUpDouble();
			DestroyObject(gameObject);
			break;
		case "slow":
			ControlBurbujas.PowerID = 1;
			ControlBurb.PowerUpSlow();
			DestroyObject(gameObject);
			break;
		}
	}
	
	void OnTriggerEnter2D(Collider2D hitObj){
		if (hitObj.gameObject.tag == "EndLine") {
			DestroyObject(gameObject);
		}
	}

}