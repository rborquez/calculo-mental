﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BorradorObj : MonoBehaviour {
		
		public string Resultado;//es la respuesta del problema en el objeto
		private Text RespuestaEscrita;// es la respuesta que va a aparecer cuando den enter
		
		void Start () {
			GameObject Respuesta = GameObject.Find ("RespuestaEscrita");
			RespuestaEscrita = Respuesta.GetComponent<Text>();
			rigidbody2D.AddForce(new Vector2 (0, -1) * ControlBurbujas.BurbujaSpeed);
		}
		
		void Update () {
			if(Resultado == RespuestaEscrita.text){
				RespuestaEscrita.text = "DELALL";
				StartCoroutine (RespuestaTemp ());
				ControlBurbujas.puntuacion ++;
			}
			
			if(ControlBurbujas.GameOver == 1)
				DestroyObject(gameObject);
		}
		


		private IEnumerator RespuestaTemp(){
			yield return new WaitForSeconds (0.1f);
			RespuestaEscrita.text = "";
			DestroyObject(gameObject);
		}

		
		void OnTriggerEnter2D(Collider2D hitObj){
			if (hitObj.gameObject.tag == "EndLine") {
				DestroyObject(gameObject);
				ControlBurbujas.vida --;
			}
		}
	}