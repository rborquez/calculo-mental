﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlNivel3 : MonoBehaviour {
	
	public static int GameOver;
	public static int puntuacion; //el puntuaje del jugador
	public static int vida; //el indicador de vidas
	public static int TimeUp;//si se acabo el tiempo

	public LogObj[] LogPref;
	private Vector3 SpawnPosition;
	GameObject spawnedLog = null;
	
	public Text puntosText;
	public Text vidasText;

	public Image bien;
	public Image mal;

	public Text PuntosFinal;
	public Text Record;
	
	public Animator GameoverAnim;
	public Animator RevivirAnim;//
	private int revivido;//
	//timer
	public Text TimerText;
	private float Timer;
	private float Duracion;
	private Text RespuestaEscrita;// es la respuesta que va a aparecer cuando den enter
	private int RandomLog;
	private int flag;//para que en el update no se haga muchas veces
	
	void Start(){
		GameObject Respuesta = GameObject.Find ("RespuestaEscrita");
		RespuestaEscrita = Respuesta.GetComponent<Text>();
		SpawnPosition = new Vector3 (0, 2.20f, 0);
		vida = 3;
		TimeUp = 0;
		puntuacion = 0;
		GameOver = 0;
		Duracion = 60;
		Timer = 60;
		bien.enabled = false;
		mal.enabled = false;
		StartCoroutine(ShowLog ());
	}
	
	void Update(){
		puntosText.text = ("" + puntuacion);
		TimerText.text = ("00:" + (int)Timer);
		
		switch (vida) {
		case 1:
			vidasText.text = " i";
			break;
		case 2:
			vidasText.text = " i i";
			break;
		case 3:
			vidasText.text = " i i i";
			break;
		default:
			vidasText.text = "";
			break;
		}
		
		if(puntuacion >= 30)
			Duracion = 40;
		else if(puntuacion >= 20)
			Duracion = 50;
		else if (puntuacion >= 8)
			Duracion = 60;
		
		//Game Over-----
		if (vida <=0){
			GameOver = 1;
			GameoverAnim.SetBool ("GameEnd", true);
			if(puntuacion > PlayerPrefs.GetInt ("RECORD3")){
				PlayerPrefs.SetInt ("RECORD3", puntuacion);
			}
			PuntosFinal.text = ("" + puntuacion);
			Record.text = ("" +PlayerPrefs.GetInt ("RECORD3"));
		}

		//control de timer--
		if (Timer <= 0) {
			if (flag == 0){
				flag = 1;
				TimeUp = 1;
				mal.enabled = true;
				StartCoroutine(ShowLog ());
			}
		}
		else {
			if(GameOver == 0)
			Timer -= Time.deltaTime;
		}

		// comparar respuesta-------------
		if(RespuestaEscrita.text != ""){
			if(LogPref[RandomLog].GetResultado() == RespuestaEscrita.text){
				if (flag == 0){
					flag = 1;
					bien.enabled = true;
					StartCoroutine(ShowLog ());
				}
			}
			else{
				if (flag == 0){
					TimeUp = 1;
					flag = 1;
					mal.enabled = true;
					StartCoroutine(ShowLog ());
				}
			}
		}
		//--------------------------------
		
		
	}
	//metodo en el que se crean los logaritmos-------------------
	private IEnumerator ShowLog(){ 
		RandomLog = Random.Range (0, LogPref.Length);
		yield return new WaitForSeconds (0.5f);
		bien.enabled = false;
		mal.enabled = false;
		Timer = Duracion;
		spawnedLog = Instantiate (LogPref[RandomLog], SpawnPosition, Quaternion.identity) as GameObject;
		flag = 0;
		TimeUp = 0;
	}	
	//---------------

	public void GotoMenu(){
		Application.LoadLevel ("MainMenu");
	}
	
	public void Jugar(){
		Application.LoadLevel ("Nivel3");
	}
}