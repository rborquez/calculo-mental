﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class instructivo : MonoBehaviour {

	private int contador;
	public Image bloqueador;
	public Text puntos;
	public Text RespuestaEscrita;
	public Image botonSiguiente; //boton para seguir
	public Text instrucciones; //texto de las instrucciones
	public Image power;
	public Image flecha;
	public Image flecha2;
	public Text tiempo;
	public Image gis;


	void Start () {
		contador = 0;
		flecha.enabled = false;
		flecha2.enabled = false;
		power.enabled = false;
		gis.enabled = false;
	}

	void Update () {
		if (RespuestaEscrita.text == "2") {
			puntos.text = "1";
			gis.enabled = false;
			contador = 3;
		}

		switch (contador) {
		case 1:
			instrucciones.text = "cada cierto tiempo caerán gises con un logaritmo que deberás resolver...";
			gis.enabled = true;
			break;
		case 2:
			instrucciones.text = "para resolverlo teclea la respuesta correcta y presiona ENTER (respueta: 2)";
			botonSiguiente.enabled = false;
			bloqueador.enabled = false;
			break;
		case 3:
			instrucciones.text = "al resolver el logaritmo el gis desaparecerá y obtendrás un punto...";
			botonSiguiente.enabled = true;
			bloqueador.enabled = true;
			break;
		case 4:
			instrucciones.text = "puedes eliminar varios gises al mismo tiempo si tienen la misma respuesta...";
			break;
		case 5:
			instrucciones.text = "si un gis llega a esta línea perderás una vida, al perder tres vidas terminara el juego...";
			flecha.enabled = true;
			break;
		case 6:
			instrucciones.text = "de vez en cuando aparecerán power-ups que te ayudaran a sobrevivir...";
			flecha.enabled = false;
			break;
		case 7:
			instrucciones.text = "para usarlos, solo tócalos con tu dedo y se activaran";
			botonSiguiente.enabled = false;
			power.enabled = true;
			break;
		case 8:
			instrucciones.text = "podrás ver el tiempo que dura ese poder aquí...";
			botonSiguiente.enabled = true;
			flecha2.enabled = true;
			tiempo.text = ".0";
			power.enabled = false;
			break;
		case 9:
			instrucciones.text = "eso es todo lo que necesitas saber, es hora de demostrar tus habilidades.";
			flecha2.enabled = false;
			break;
		case 10:
			PlayerPrefs.SetInt("FirstTime", 1);
			Application.LoadLevel("MainMenu");
			break;

		}
	}


	public void Siguiente(){
		contador ++;
	}

	public void Saltar(){
		PlayerPrefs.SetInt("FirstTime", 1);
		Application.LoadLevel("MainMenu");
	}
}
