﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class teclado : MonoBehaviour {

	public InputField Espacio_escribir;
	public Text RespuestaEscritaT;// es la respuesta que va a aparecer cuando den enter

	public void Escribir(string numero){
		Espacio_escribir.text += numero; 
	}

	public void Borrar(){
		Espacio_escribir.text = Espacio_escribir.text.Substring(0, Espacio_escribir.text.Length - 1);
	}

	public void Enter(){ //cuando das enter la respuesta guarda temporalmente y se compara, luego se borra 
		RespuestaEscritaT.text = Espacio_escribir.text;
		Espacio_escribir.text = "";
		StartCoroutine (RespuestaTemp ());
	}

	private IEnumerator RespuestaTemp(){
		yield return new WaitForSeconds (0.1f);
		RespuestaEscritaT.text = "";
	}


}
