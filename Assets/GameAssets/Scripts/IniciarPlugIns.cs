﻿using UnityEngine;
using System.Collections;

public class IniciarPlugIns : MonoBehaviour {

	void Start(){
		//AudioListener.volume = PlayerPrefs.GetInt ("Sound");
		StartCoroutine (GoToGame ());
	}

	private IEnumerator GoToGame(){
		Debug.Log("counting");
		yield return new WaitForSeconds (3);
		if(PlayerPrefs.GetInt("FirstTime")!=1)
			Application.LoadLevel ("Instrucciones");
		else	
			Application.LoadLevel ("MainMenu");
	}
	
}