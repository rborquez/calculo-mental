﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlBurbujas : MonoBehaviour {

	public static int GameOver;
	public static float BurbujaSpeed; //la velocidad con la que caen
	public static int BurbujaTime; //el tiempo en que tardan en salir
	public static int puntuacion; //el puntuaje del jugador
	public static int vida; //el indicador de vidas
	public static int PowerID; //0. none 1. slow  2. double 3. freeze

	public PowerUpObj[] PUprefab; 
	public BurbujaObj[] BurbujaPrefab;
	public int Nivel;
	public Vector3 SpawnPosition;
	private Vector3 PUSpawnPos;
	GameObject spawnedBurb = null;
	GameObject spawnedPower = null;
	private int RandomTime, XRandomPos;
	private float TemporalSpeed;

	public Text puntosText;
	public Text vidasText;

	public Text PuntosFinal;
	public Text Record;

	public Animator GameoverAnim;
	public Image FrozeImg;
	//timer
	public Text TimerText;
	private float Timer;

	void Start(){
		StartCoroutine (BurbujaFall ());
		StartCoroutine (PowerUpsFall ());
		PowerID = 0;
		BurbujaSpeed = 25f;
		BurbujaTime = 40;
		vida = 3;
		puntuacion = 0;
		FrozeImg.enabled = false;
		GameOver = 0;
		Timer = 0;
		SoundManager.instance.musicSource.Play();
	}

	void Update(){
		puntosText.text = ("" + puntuacion);
		TimerText.text = (":" + (int)Timer);

		switch (vida) {
		case 1:
			vidasText.text = " i";
			break;
		case 2:
			vidasText.text = " i i";
			break;
		case 3:
			vidasText.text = " i i i";
			break;
		default:
			vidasText.text = "";
			break;
		}

		if(puntuacion >= 30)
			BurbujaTime = 10;
		else if(puntuacion >= 20)
			BurbujaTime = 20;
		else if (puntuacion >= 8)
			BurbujaTime = 30;
		

		if (vida <=0){
			GameOver = 1;
			GameoverAnim.SetBool ("GameEnd", true);

			SoundManager.instance.musicSource.Stop();
			if(puntuacion > PlayerPrefs.GetInt ("RECORD" +Nivel)){
				PlayerPrefs.SetInt ("RECORD" +Nivel, puntuacion);
			}
			PuntosFinal.text = ("" + puntuacion);
			Record.text = ("" +PlayerPrefs.GetInt ("RECORD" +Nivel));
		}

		if (Timer <= 0) {
			TimerText.enabled = false;
		}
		else {
			TimerText.enabled = true;
			Timer -= Time.deltaTime;
		}


	}

	private IEnumerator BurbujaFall(){ //loop donde se crean las burbujas-------------------
		RandomTime = Random.Range (BurbujaTime, BurbujaTime+20);
		XRandomPos = Random.Range (-2,3);
		yield return new WaitForSeconds (RandomTime/10);
		if (GameOver == 0) {
			SpawnPosition = new Vector3 (XRandomPos, 5.50f, 0);
			spawnedBurb = Instantiate (BurbujaPrefab[Random.Range(0,BurbujaPrefab.Length)], SpawnPosition, Quaternion.identity) as GameObject;
			StartCoroutine(BurbujaFall());
		}
	}

	private IEnumerator BurbujaFallRev(){ //loop donde se crean las burbujas-------------------
		RandomTime = Random.Range (BurbujaTime, BurbujaTime+20);
		XRandomPos = Random.Range (-2,3);
		yield return new WaitForSeconds (RandomTime/10);
		if (GameOver == 0){
			SpawnPosition = new Vector3 (XRandomPos, 5.50f, 0);
			spawnedBurb = Instantiate (BurbujaPrefab[Random.Range(0,BurbujaPrefab.Length)], SpawnPosition, Quaternion.identity) as GameObject;
			StartCoroutine(BurbujaFallRev());
		}
	}

	private IEnumerator PowerUpsFall(){//loop para power ups---------------------------------

		int PURandomTime = Random.Range (200, 300);
		float RandPosX =  Random.Range (-2.1f, 2.12f);
		yield return new WaitForSeconds (PURandomTime/10);
		if (GameOver != 1) {
			PUSpawnPos = new Vector3 (RandPosX, 5.50f, 0);
			spawnedPower = Instantiate (PUprefab[Random.Range(0,PUprefab.Length)], PUSpawnPos, Quaternion.identity) as GameObject;
			StartCoroutine (PowerUpsFall());
		}
	}


//----------------------------- POWER UPS --------------------

//1
	public void PowerUpFreeze(){
		FrozeImg.enabled = true;
		GameOver = 2;
		Timer = 5;
		StartCoroutine(PowerUpFreeze(5));
	}
	
	private IEnumerator PowerUpFreeze(int PUtime){ // se activa el power up el tiempo dado
		yield return new WaitForSeconds (PUtime);
		FrozeImg.enabled = false;
		GameOver = 0;
		StartCoroutine (BurbujaFall ());
		PowerID = 0;
	}

//2
	public void PowerUpDouble(){
		Timer = 10;
		StartCoroutine(PowerUpDouble(10));
	}

	private IEnumerator PowerUpDouble(int PUtime){ // se activa el power up el tiempo dado
		yield return new WaitForSeconds (PUtime);
		PowerID = 0;
	}

//3
	public void PowerUpSlow(){
		Timer = 10;
		StartCoroutine(PowerUpSlow(10));
	}

	private IEnumerator PowerUpSlow(int PUtime){ // se activa el power up el tiempo dado
		TemporalSpeed = BurbujaSpeed;
		BurbujaSpeed = BurbujaSpeed/2;
		yield return new WaitForSeconds (PUtime);
		BurbujaSpeed = TemporalSpeed;
		PowerID = 0;

	}

//--------------------------------------
	public void StartBurbujaFall(){
		StartCoroutine (BurbujaFall ());
	}

	public void GotoMenu(){
		Application.LoadLevel ("MainMenu");
	}

	public void Jugar(){
		Application.LoadLevel ("MainGame");
	}

}
