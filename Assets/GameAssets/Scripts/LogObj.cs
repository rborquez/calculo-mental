﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LogObj : MonoBehaviour {
	
	public string Resultado;//es la respuesta del problema en el objeto
	private Text RespuestaEscrita;// es la respuesta que va a aparecer cuando den enter
	
	void Start () {
		GameObject Respuesta = GameObject.Find ("RespuestaEscrita");
		RespuestaEscrita = Respuesta.GetComponent<Text>();
	}
	
	void Update () {
		if(Resultado == RespuestaEscrita.text){
			DestroyObject(gameObject);
			ControlNivel3.puntuacion ++;
		}

		if(ControlNivel3.GameOver == 1)
			DestroyObject(gameObject);

		if (ControlNivel3.TimeUp == 1) {
			ControlNivel3.vida --;
			DestroyObject (gameObject);
		}
	}

	public string GetResultado(){
		return Resultado;
	}
}
